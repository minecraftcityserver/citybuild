package de.evilshark.rpg.common;

public class MoneyManager {
	
	public static int getMoneyOnHand(String player) {
		
		Main.loadBank();
		
		return Main.finance.getInt(player.toLowerCase() + ".onHand");
		
	}
	
	public static int getMoneyOnBank(String player) {
		
		Main.loadBank();
		
		return Main.finance.getInt(player.toLowerCase() + ".onBank");
	}
	
	public static void addMoney(String player, int amount) {
		
		Main.loadBank();
		
		int old = Main.finance.getInt(player.toLowerCase() + ".onBank");
		int after = old + amount;
		
		Main.finance.set(player.toLowerCase() + ".onBank", after);
		Main.saveBank();
		
	}
	
	public static void setMoney(String player, int amount) {
	
		Main.finance.set(player.toLowerCase() + ".onBank", amount);
		Main.saveBank();
		
	}
	
	public static void removeMoney(String player, int amount) {
		
		Main.loadBank();
		int old = Main.finance.getInt(player.toLowerCase() + ".onBank");
		int after = old - amount;
		
		Main.finance.set(player.toLowerCase() + ".onBank", after);
		Main.saveBank();
		
	}
	
	public static void wireMoney(String player1, String player2, int amount) {
		
		int old1 = getMoneyOnBank(player1);
		int old2 = getMoneyOnBank(player2);
		
		int after1 = old1 - amount;
		int after2 = old2 + amount;
		
		setMoney(player1, after1);
		setMoney(player2, after2);
		
	}

}
