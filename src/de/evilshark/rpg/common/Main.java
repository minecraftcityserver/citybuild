package de.evilshark.rpg.common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;


@SuppressWarnings({ "unchecked", "rawtypes", })

public class Main extends JavaPlugin {
	
	public static Main main;
	
	public static Main getInstance() {
		
		return main;
		
	}
	  public static File cfile = new File("plugins/SharkRPGReloaded", "config.yml");
	  public static FileConfiguration config = YamlConfiguration.loadConfiguration(cfile);
	
	  public static File file = new File("plugins/SharkRPGReloaded", "playerdata.yml");
	  public static FileConfiguration playerdata = YamlConfiguration.loadConfiguration(file);
	  
	  public static File file2 = new File("plugins/SharkRPGReloaded", "drug.yml");
	  public static FileConfiguration drugs = YamlConfiguration.loadConfiguration(file2);
	  
	  public static File file3 = new File("plugins/SharkRPGReloaded", "finance.yml");
	  public static FileConfiguration finance = YamlConfiguration.loadConfiguration(file3);
	  
	  public static File file4 = new File("plugins/SharkRPGReloaded", "quicktravel.yml");
	  public static FileConfiguration quicktravel = YamlConfiguration.loadConfiguration(file4);
	  
	  public static File file5 = new File("plugins/SharkRPGReloaded", "jobs.yml");
	  public static FileConfiguration jobs = YamlConfiguration.loadConfiguration(file5);
	  
	  public static File file6 = new File("plugins/SharkRPGReloaded", "hotel.yml");
	  public static FileConfiguration hotel = YamlConfiguration.loadConfiguration(file6);
	  
	  public static File file7 = new File("plugins/SharkRPGReloaded", "houses.yml");
	  public static FileConfiguration houses = YamlConfiguration.loadConfiguration(file7);
	  
	  
	public static HashMap<Player, Player> dial = new HashMap();
	public static HashMap<Player, Player> talk = new HashMap();
	public static HashMap<Player, String> buyhouses = new HashMap();
	public static final ArrayList emergency = new ArrayList();
	public static final ArrayList hotelguest = new ArrayList();
	
	@Override
	public void onEnable() {
		
		
		
		loadConfig();
		loadCFile();
		testStats();
		testBank();
		testDrugs();
		testHotel();
		testHouses();
		testQuicktravel();
		testJobs();
		startMsg();
		
	}
	
	public static String prefix = "�1"+config.getString("general.prefix")+" �8>> ";
	public static String error = prefix + "�4ERROR: �c";
	public static String argument = prefix + "�4ERROR: �cPlease use the correct arguments!";
	public static String console = prefix + "�4ERROR: �cYou cant execute this command as console!";
	public static String permission = prefix + "�4ERROR: �cSie haben nicht gen�gend Zugriffsrechte f�r diesen Command!";
	public static String offline = prefix + "�4ERROR: �cDieser Spieler wurde nicht gefunden!";
	
	private void startMsg() {
		
		System.out.println(prefix+"The RPG Plugin version �5"+this.getDescription().getVersion()+" �8by �4_EvilShark_ �8was started!");
		
		for(Player online : Bukkit.getOnlinePlayers()) {
			
			if(online.hasPermission("rpg.admin.status")) {
				
				online.sendMessage(prefix+"The RPG Plugin version �5"+this.getDescription().getVersion()+" �8by �4_EvilShark_ �8was started!");
				
			}
			
		}
		
	}
	
	
	private void testStats() {
		
		if(!(file.exists())) {
			
			try {
				file.createNewFile();
			} catch (IOException e) {

				e.printStackTrace();
			}
			
		}
		
	}
	
	private void testDrugs() {
		
		if(!(file2.exists())) {
			
			try {
				file2.createNewFile();
			} catch (IOException e) {
				 
				e.printStackTrace();
			}
			
		}
	}
	
	private void testBank() {
	
		if(!(file3.exists())) {
			
			try {
				file3.createNewFile();
			} catch (IOException e) {
				 
				e.printStackTrace();
			}
			
		}
		
	}
	
	private void testQuicktravel() {
		
		if(!(file4.exists())) {
			
			try {
				file4.createNewFile();
			} catch (IOException e) {
				 
				e.printStackTrace();
			}
			
		}
	
	}
	
	private void testJobs() {
		
		if(!(file5.exists())) {
			
			try {
				file5.createNewFile();
			} catch (IOException e) {
				 
				e.printStackTrace();
			}
		
			setupJobs();
			
		}
		
	}
	
	private void testHotel() {
		
		if(!(file6.exists())) {
			
			try {
				file6.createNewFile();
			} catch (IOException e) {
				 
				e.printStackTrace();
			}
		
			setupHotel();
			
		}
		
	}
	
	private void testHouses() {
		
		if(!(file7.exists())) {
			
			try {
				file7.createNewFile();
			} catch (IOException e) {
				 
				e.printStackTrace();
			}
			
		}
		
	}
	
	public static void loadStats() {
		
		try {
			playerdata.load(file);
		} catch (IOException | InvalidConfigurationException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void loadBank() {
		
		try {
			finance.load(file3);
		} catch (IOException | InvalidConfigurationException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void loadQuicktravel() {
		
		try {
			quicktravel.load(file4);
		} catch (IOException | InvalidConfigurationException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void loadJobs() {
		
		try {
			jobs.load(file5);
		} catch (IOException | InvalidConfigurationException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void loadHotel() {
		
		try {
			hotel.load(file6);
		} catch (IOException | InvalidConfigurationException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void loadHouses() {
		
		try {
			houses.load(file7);
		} catch (IOException | InvalidConfigurationException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void loadDrugs() {
		
		try {
			drugs.load(file2);
		} catch (IOException | InvalidConfigurationException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void saveStats() {
		
		try {
			playerdata.save(file);
		} catch (IOException e) {
			 
			e.printStackTrace();
		}
	
	}
	
	public static void saveBank() {
		
		try {
			finance.save(file3);
		} catch (IOException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void saveQuicktravel() {
		
		try {
			quicktravel.save(file4);
		} catch (IOException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void saveJobs() {
		
		try {
			jobs.save(file5);
		} catch (IOException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void saveHotel() {
		
		try {
			jobs.save(file6);
		} catch (IOException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void saveHouses() {
		
		try {
			houses.save(file7);
		} catch (IOException e) {
			 
			e.printStackTrace();
		}
		
	}
	
	public static void saveDrugs() {
		
		try {
			drugs.save(file2);
		} catch (IOException e) {
			 
			e.printStackTrace();
		}
	
	}
	
	private void setupHotel() {
		
		hotel.set("101.isFree", Boolean.valueOf(true));
	    hotel.set("102.isFree", Boolean.valueOf(true));
	    hotel.set("103.isFree", Boolean.valueOf(true));
	    hotel.set("104.isFree", Boolean.valueOf(true));
	    hotel.set("105.isFree", Boolean.valueOf(true));
	    hotel.set("106.isFree", Boolean.valueOf(true));
	    
	    hotel.set("201.isFree", Boolean.valueOf(true));
	    hotel.set("202.isFree", Boolean.valueOf(true));
	    hotel.set("203.isFree", Boolean.valueOf(true));
	    hotel.set("204.isFree", Boolean.valueOf(true));
	    hotel.set("205.isFree", Boolean.valueOf(true));
	    hotel.set("206.isFree", Boolean.valueOf(true));
	    
	    hotel.set("301.isFree", Boolean.valueOf(true));
	    hotel.set("302.isFree", Boolean.valueOf(true));
	    hotel.set("303.isFree", Boolean.valueOf(true));
	    hotel.set("304.isFree", Boolean.valueOf(true));
	    hotel.set("305.isFree", Boolean.valueOf(true));
	    hotel.set("306.isFree", Boolean.valueOf(true));
	    
	    hotel.set("401.isFree", Boolean.valueOf(true));
	    hotel.set("402.isFree", Boolean.valueOf(true));
	    hotel.set("403.isFree", Boolean.valueOf(true));
	    hotel.set("404.isFree", Boolean.valueOf(true));
	    hotel.set("405.isFree", Boolean.valueOf(true));
	    hotel.set("406.isFree", Boolean.valueOf(true));
	    
	    hotel.set("501.isFree", Boolean.valueOf(true));
	    hotel.set("502.isFree", Boolean.valueOf(true));
	    hotel.set("503.isFree", Boolean.valueOf(true));
	    hotel.set("504.isFree", Boolean.valueOf(true));
	    hotel.set("505.isFree", Boolean.valueOf(true));
	    hotel.set("506.isFree", Boolean.valueOf(true));
	    
	    hotel.set("701.isFree", Boolean.valueOf(true));
	    hotel.set("702.isFree", Boolean.valueOf(true));
	    hotel.set("703.isFree", Boolean.valueOf(true));
	    hotel.set("704.isFree", Boolean.valueOf(true));
	    hotel.set("705.isFree", Boolean.valueOf(true));
	    hotel.set("706.isFree", Boolean.valueOf(true));
	    
	    hotel.set("801.isFree", Boolean.valueOf(true));
	    hotel.set("802.isFree", Boolean.valueOf(true));
	    hotel.set("803.isFree", Boolean.valueOf(true));
	    hotel.set("804.isFree", Boolean.valueOf(true));
	    hotel.set("805.isFree", Boolean.valueOf(true));
	    hotel.set("806.isFree", Boolean.valueOf(true));
	    
	    hotel.set("601.isFree", Boolean.valueOf(true));
	    hotel.set("602.isFree", Boolean.valueOf(true));
	    hotel.set("603.isFree", Boolean.valueOf(true));
	    hotel.set("604.isFree", Boolean.valueOf(true));
	    hotel.set("605.isFree", Boolean.valueOf(true));
	    hotel.set("606.isFree", Boolean.valueOf(true));
	    
	    hotel.set("901.isFree", Boolean.valueOf(true));
	    hotel.set("902.isFree", Boolean.valueOf(true));
	    hotel.set("903.isFree", Boolean.valueOf(true));
	    hotel.set("904.isFree", Boolean.valueOf(true));
	    hotel.set("905.isFree", Boolean.valueOf(true));
	    hotel.set("906.isFree", Boolean.valueOf(true));
	    
	    saveHotel();
		
	}
	
	private void setupJobs() {
		
		jobs.set("Police.Payout", 0);
		saveJobs();
		
	}

	
	
	
	
	
	private void loadConfig() {
		
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	}
	
	private void loadCFile() {
		
		try {
			config.load(cfile);
		} catch (IOException | InvalidConfigurationException e) {
			
			e.printStackTrace();
		}
		
	}
	
	
	
	
	

}
