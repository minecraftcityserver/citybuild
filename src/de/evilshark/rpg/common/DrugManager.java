package de.evilshark.rpg.common;

public class DrugManager {
	
	public static void addDrugs(String player, String drug) {
		
		Main.loadDrugs();
		int before = Main.drugs.getInt(player + "." + drug);
		int after = before + 1;
		
		Main.drugs.set(player + "." + drug, after);
		Main.saveDrugs();
		
	}
	
	public static void removeDrugs(String player, String drug) {
		
		Main.loadDrugs();
		int before = Main.drugs.getInt(player + "." + drug);
		int after = before - 1;
		
		Main.drugs.set(player + "." + drug, after);
		Main.saveDrugs();
	
	}
	
	public static int getDrugs(String player, String drug) {
		
		Main.loadDrugs();
		
		return Main.drugs.getInt(player + "." + drug);
	}

}
