package de.evilshark.rpg.crime;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import de.evilshark.rpg.common.DrugManager;
import de.evilshark.rpg.common.Main;
import de.evilshark.rpg.common.MoneyManager;

public class DrugShop implements Listener {
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		
		Player p = (Player) e.getWhoClicked();
		int money = MoneyManager.getMoneyOnHand(p.getName());
		
		String inventory = e.getInventory().getName();
		Material clicked = e.getCurrentItem().getType();
		
	    Material heroin = Material.GLOWSTONE_DUST;
	    Material koks = Material.SUGAR;
	    Material grass = Material.GRASS;
	    Material meth = Material.REDSTONE;
	    
	    
	    int buyKoks = Main.config.getInt("crime-drugdealing.buy-koks");
	    int buyMeth = Main.config.getInt("crime-drugdealing.buy-meth");
	    int buyHeroin = Main.config.getInt("crime-drugdealing.buy-heroin");
	    int buyWeed = Main.config.getInt("crime-drugdealing.buy-weed");
	    
	    int sellKoks = Main.config.getInt("crime-drugdealing.sell-koks");
	    int sellMeth = Main.config.getInt("crime-drugdealing.sell-meth");
	    int sellHeroin = Main.config.getInt("crime-drugdealing.sell-heroin");
	    int sellWeed = Main.config.getInt("crime-drugdealing.sell-weed");
	    
	    if(inventory.equalsIgnoreCase("�6Buy Drugs")) {
	    	
	    	if(clicked.equals(heroin)) {
	    		
	    		if(money >= buyHeroin) {
	    			
	    			DrugManager.addDrugs(p.getName(), "heroin");
	    			
	    		} else {
	    			
	    			p.closeInventory();
	    			p.sendMessage("�1Drugdealing �8>> �cDu hast nicht gen�gend Geld dabei!");
	    			
	    		}
	    		
	    	} else
	    	
	    	if(clicked.equals(koks)) {
	    		
	    		if(money >= buyKoks) {
	    			
	    			DrugManager.addDrugs(p.getName(), "koks");
	    			
	    		} else {
	    			
	    			p.closeInventory();
	    			p.sendMessage("�1Drugdealing �8>> �cDu hast nicht gen�gend Geld dabei!");
	    			
	    		}
	    		
	    	} else
	    	
	    	if(clicked.equals(grass)) {
	    		
	    		if(money >= buyWeed) {
	    			
	    			DrugManager.addDrugs(p.getName(), "weed");
	    			
	    		} else {
	    			
	    			p.closeInventory();
	    			p.sendMessage("�1Drugdealing �8>> �cDu hast nicht gen�gend Geld dabei!");
	    			
	    		}
	    		
	    	} else
	    	
	    	if(clicked.equals(meth)) {
	    		
	    		if(money >= buyMeth) {
	    			
	    			DrugManager.addDrugs(p.getName(), "meth");
	    			
	    		} else {
	    			
	    			p.closeInventory();
	    			p.sendMessage("�1Drugdealing �8>> �cDu hast nicht gen�gend Geld dabei!");
	    			
	    		}
	    		
	    	}
	    	
	    	
	    	e.setCancelled(true);
	    	
	    } else if(inventory.equalsIgnoreCase("�6Sell Drugs")) {
	    	
	    	if(clicked.equals(heroin)) {
	    		
	    		if(DrugManager.getDrugs(p.getName(), "heroin") >= 1) {
	    			
	    			DrugManager.removeDrugs(p.getName(), "heroin");
	    			MoneyManager.addMoney(p.getName(), sellHeroin);
	    			
	    		} else {
	    			
	    			p.closeInventory();
	    			p.sendMessage("�1DrugDealing �8>> �cDu hast keine Drogen zum Verkaufen!");
	    			
	    		}
	    		
	    		
	    	} else
	    	
	    	if(clicked.equals(koks)) {
	    		
	    		if(DrugManager.getDrugs(p.getName(), "koks") >= 1) {
	    			
	    			DrugManager.removeDrugs(p.getName(), "koks");
	    			MoneyManager.addMoney(p.getName(), sellKoks);
	    			
	    		} else {
	    			
	    			p.closeInventory();
	    			p.sendMessage("�1DrugDealing �8>> �cDu hast keine Drogen zum Verkaufen!");
	    			
	    		}
	    		
	    		
	    	} else
	    	
	    	if(clicked.equals(grass)) {
	    		
	    		if(DrugManager.getDrugs(p.getName(), "weed") >= 1) {
	    			
	    			DrugManager.removeDrugs(p.getName(), "weed");
	    			MoneyManager.addMoney(p.getName(), sellWeed);
	    			
	    		} else {
	    			
	    			p.closeInventory();
	    			p.sendMessage("�1DrugDealing �8>> �cDu hast keine Drogen zum Verkaufen!");
	    			
	    		}
	    		
	    		
	    	} else
	    	
	    	if(clicked.equals(meth)) {
	    		
	    		if(DrugManager.getDrugs(p.getName(), "meth") >= 1) {
	    			
	    			DrugManager.removeDrugs(p.getName(), "meth");
	    			MoneyManager.addMoney(p.getName(), sellMeth);
	    			
	    		} else {
	    			
	    			p.closeInventory();
	    			p.sendMessage("�1DrugDealing �8>> �cDu hast keine Drogen zum Verkaufen!");
	    			
	    		}
	    		
	    		
	    	}
	    	
	    	
	    	
	    	e.setCancelled(true);
	    	
	    }
		
	    
		
	}

}
